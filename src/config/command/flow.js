const database = require('../../database/models')

const { mockTicket } =
  require('../../api/tickets/v1/command/console')

const mock = async () => {
  await mockTicket(20)
}

mock().then(() => {
  setTimeout(() => {
    database.sequelize.close()
  }, 3000)
})
