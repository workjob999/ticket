module.exports = {
  HEADERS: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'Authorization',
    'AccessToken',
    'Cache-Control',
    'X-CSRF-Token',
    'Content-Length'
  ]
}
