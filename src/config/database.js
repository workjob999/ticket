require('dotenv').config('/')

module.exports = {
  local: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    pool: {
      min: 2,
      max: 10
    },
    dialect: 'mysql',
    migrationStorage: 'sequelize',
    migrationStorageTableName: 'sequelize_meta',
    timezone: '+00:00',
    logging: false,
    use_env_variable: false
  },

  test: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME_TEST,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    pool: {
      min: 2,
      max: 10
    },
    dialect: 'mysql',
    migrationStorage: 'sequelize',
    migrationStorageTableName: 'sequelize_meta',
    timezone: '+00:00',
    logging: false,
    use_env_variable: false
  },

  develop: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    pool: {
      min: 2,
      max: 10
    },
    dialect: 'mysql',
    migrationStorage: 'sequelize',
    migrationStorageTableName: 'sequelize_meta',
    timezone: '+00:00',
    logging: false,
    use_env_variable: true
  },

  staging: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    pool: {
      min: 2,
      max: 10
    },
    dialect: 'mysql',
    migrationStorage: 'sequelize',
    migrationStorageTableName: 'sequelize_meta',
    timezone: '+00:00',
    logging: false
  },

  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    pool: {
      min: 2,
      max: 10
    },
    dialect: 'mysql',
    migrationStorage: 'sequelize',
    migrationStorageTableName: 'sequelize_meta',
    timezone: '+00:00',
    logging: false
  }
}
