module.exports = {
  SUCCESS: {
    OK: {
      CODE: 200,
      MESSAGE: 'ok'
    },
    OTP_SUCCESS: {
      CODE: 200,
      MESSAGE: 'otp_found'
    },
    OTP_SENT_SUCCESS: {
      CODE: 200,
      MESSAGE: 'otp_was_sent'
    }
  },
  CLIENT_ERROR: {
    NOT_FOUND: {
      CODE: 404,
      MESSAGE: 'not_found'
    },
    BAD_REQUEST: {
      CODE: 400,
      MESSAGE: 'bad_request'
    },
    UNAUTHORIZED: {
      CODE: 401,
      MESSAGE: 'unauthorized'
    },
    FORBIDDEN: {
      CODE: 403,
      MESSAGE: 'Forbidden'
    }
  },
  SYNTAX_ERROR: {
    CODE: 400,
    STATUS: 'error',
    ERROR_STATUS: 'bad_request',
    MESSAGE: 'json_parser_failed'
  }
}
