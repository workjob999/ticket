module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('tickets', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: Sequelize.UUID
    },
    fullname: {
      type: Sequelize.STRING,
      allowNull: true
    },
    detail: {
      allowNull: true,
      type: Sequelize.TEXT
    },
    contact_information: {
      allowNull: true,
      type: Sequelize.TEXT
    },
    status: {
      allowNull: true,
      type: Sequelize.ENUM('Pending', 'Accepted', 'Resolved', 'Rejected'),
      defaultValue: 'Pending'
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  }),
  down: (queryInterface) => queryInterface.dropTable('tickets')
}
