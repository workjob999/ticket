module.exports = (sequelize, DataTypes) => {
  const Tickets = sequelize.define('tickets', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      unique: true,
      defaultValue: DataTypes.UUIDV4
    },
    fullname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    detail: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    contact_information: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    status: {
      allowNull: true,
      type: DataTypes.ENUM('Pending', 'Accepted', 'Resolved', 'Rejected'),
      defaultValue: 'Pending'
    }
  }, {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  })

  return Tickets
}
