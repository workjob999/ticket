const { port } = require('./config/app')
const { HEADERS } = require('./config/allow-headers')

const ExpressApp = require('../infrastructure/express-app')
const ExpressServer = require('../infrastructure/express-server')

const expressApp = new ExpressApp()
expressApp.init()
expressApp.allowHeaders(HEADERS.toString())

const expressServer = new ExpressServer(expressApp.getApp(), { port })
expressServer.start()

const shutdown = () => {
  expressServer.server.close()

  setTimeout(() => {
    process.exit(1)
  }, 3000)
}

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)

module.exports = expressServer.server
