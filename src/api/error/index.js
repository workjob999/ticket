const ERROR_MESSAGE = require('../error/message')

class ErrorHandle extends Error {
  constructor (error, responseMessage) {
    super()
    this.error = error
    this.responseMessage = responseMessage
  }

  getResponseMessage () {
    if (!this.responseMessage) {
      return ERROR_MESSAGE.BAD_REQUEST
    }
    return this.responseMessage
  }
}

module.exports = ErrorHandle
