const Sentry = require('@sentry/node')
const HTTP_MESSAGE = require('../../config/http-message')
const { environment } = require('../../config/app')
const API_RESPONSE_MESSAGE = require('../response-message')

const ErrorHandle = require('../error/index')
const ERROR_MESSAGE = require('../error/message')

class ErrorApiManager {
  static handle (err, res) {
    this.err = err

    if (!(err instanceof ErrorHandle)) {
      this.err = new ErrorHandle(err, ERROR_MESSAGE.BAD_REQUEST)
    }

    if (environment !== 'test') {
      Sentry.captureException(err)
    }

    return res.status(HTTP_MESSAGE.SUCCESS.OK.CODE).json({
      code: HTTP_MESSAGE.CLIENT_ERROR.BAD_REQUEST.CODE,
      status: API_RESPONSE_MESSAGE.NOT_SUCCESS.MESSAGE,
      errorStatus: this.err.getResponseMessage().STATUS,
      errorMessage: this.err.getResponseMessage().MESSAGE
    })
  }
}

module.exports = ErrorApiManager
