const express = require('express')

const router = express.Router()

const tickets = require('../api/tickets/v1/route')

router.use('/v1', tickets)

module.exports = router
