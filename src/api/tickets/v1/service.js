const Repository = require('./repository')
const APP_VARIABLES = require('../../../config/variables')
const { Op } = require('sequelize')

class TicketServices {
  async getTickets (conditionTicket) {
    const {
      status = null,
      startDate = null,
      endDate = null,
      order = 'created_at',
      ascdesc = 'DESC',
      limit = APP_VARIABLES.LIMIT,
      page = 1
    } = conditionTicket

    const filter = []

    const whereStatus = []
    if (status) {
      Object.assign(whereStatus, {
        status
      })
    }

    const whereDate = []
    if (startDate && endDate) {
      Object.assign(whereDate, {
        created_at: {
          [Op.between]: [startDate, endDate]
        }
      })
    } else if (startDate) {
      Object.assign(whereDate, {
        created_at: {
          [Op.lte]: startDate
        }
      })
    } else if (endDate) {
      Object.assign(whereDate, {
        created_at: {
          [Op.gte]: endDate
        }
      })
    }

    if (whereStatus) {
      Object.assign(filter, whereStatus)
    }

    if (whereDate) {
      Object.assign(filter, whereDate)
    }

    const offset = ((page - 1) * limit)
    const limitPage = parseInt(limit)

    const orderBy = [order, ascdesc]

    const tickets = await Repository.getTickets(
      filter,
      orderBy,
      limitPage,
      offset
    )

    return tickets
  }

  async createTicket (createDataTicket) {
    return await Repository.createTicket(createDataTicket)
  }

  async updateStatus (ticketId, status = null) {
    const filter = {
      id: ticketId
    }

    const data = {
      status
    }

    const ticket = await Repository.updateTicket(filter, data)

    return ticket
  }
}

module.exports = TicketServices
