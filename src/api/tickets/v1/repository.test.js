const faker = require('faker')

const { expect } = require('chai')

const TicketRepository = require('./repository')

describe('[UNIT] test Repository', () => {
  describe('Get Tickets', () => {
    it('Should be pass when get tickets no filter', async () => {
      const tickets = await TicketRepository.getTickets()
      expect(tickets).not.to.equal(null)
    })
  })

  describe('Create ticket', () => {
    it('Should be not null when data is correct', async () => {
      const dataTicket = {
        fullname: faker.name.findName(),
        detail: faker.lorem.text(),
        contact_information: faker.address.streetAddress(),
        status: faker.random.arrayElement(['Pending', 'Accepted', 'Resolved', 'Rejected']),
        created_at: faker.date.between('2018-01-01', '2020-12-31')
      }

      const ticket = await TicketRepository.createTicket(dataTicket)

      expect(ticket).not.to.equal(null)
    })
  })

  describe('Update status ticket', () => {
    it('Should be not null when data is correct', async () => {
      const where = { status: 'Pending' }
      const getData = await TicketRepository.findTicket(where)
      const statusTicket = {
        status: faker.random.arrayElement(['Pending', 'Accepted', 'Resolved', 'Rejected'])
      }

      const filter = {
        id: getData.id
      }

      const ticket = await TicketRepository.updateTicket(filter, statusTicket)
      expect(ticket).not.to.equal(null)
    })
  })
})
