module.exports = {
  BAD_REQUEST: {
    STATUS: 'bad_request',
    MESSAGE: 'bad_request'
  },
  INVALID_PARAMS: {
    STATUS: 'bad_request',
    MESSAGE: 'invalid_params'
  },
  INVALID_STATUS: {
    STATUS: 'bad_request',
    MESSAGE: 'invalid_status'
  }
}
