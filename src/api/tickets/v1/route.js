const express = require('express')
const controller = require('./controller')

const router = express.Router()

router.get('/tickets', controller.getTickets)
router.post('/tickets', controller.createTicket)
router.patch('/tickets/:ticketId/updateStatus', controller.updateStatus)

module.exports = router
