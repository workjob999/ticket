module.exports = {
  STATUS: {
    PENDING: 'Pending',
    ACCEPTED: 'Accepted',
    RESOLVED: 'Resolved',
    REJECTED: 'Rejected'
  }
}
