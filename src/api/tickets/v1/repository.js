const APP_VARIABLES = require('../../../config/variables')

const Tickets = require('../../../database/models').tickets

class TicketRepository {
  static async getTickets (where = null,
    orderBy = ['created_at', 'DESC'],
    limit = APP_VARIABLES.LIMIT,
    offset = 1) {
    const filterPage = {
      offset,
      limit
    }

    const filter = {
      where: {},
      order: [
        orderBy
      ],
      ...filterPage
    }

    if (where) {
      Object.assign(filter.where, where)
    }

    const tickets = await Tickets.findAll(filter)

    return tickets
  }

  static async findTicket (filter) {
    const ticket = await Tickets.findOne({
      where: filter
    })

    return ticket
  }

  static async createTicket (ticketData) {
    return await Tickets.create(ticketData)
  }

  static async updateTicket (filter, data) {
    const ticket = await Tickets.update(
      data,
      {
        where: filter
      }
    )

    return ticket
  }
}

module.exports = TicketRepository
