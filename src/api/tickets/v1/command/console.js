/* eslint-disable no-await-in-loop */
const faker = require('faker')

const Repository = require('../repository')

module.exports.mockTicket = async (totalTickets) => {
  for (let i = 1; i <= totalTickets; i += 1) {
    const ticket = {
      fullname: faker.name.findName(),
      detail: faker.lorem.text(),
      contact_information: faker.address.streetAddress(),
      status: faker.random.arrayElement(['Pending', 'Accepted', 'Resolved', 'Rejected']),
      created_at: faker.date.between('2018-01-01', '2020-12-31')
    }

    await Repository.createTicket(ticket)
  }
}
