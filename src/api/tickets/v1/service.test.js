const { expect } = require('chai')
const APP_VARIABLES = require('../../../config/variables')
const faker = require('faker')
const TicketRepository = require('./repository')
const TicketServices = require('./service')

const ticketServices = new TicketServices()

describe('[UNIT] test service', () => {
  describe('Get Ticket', () => {
    const conditionTicket = {
      status: null,
      startDate: null,
      endDate: null,
      order: 'created_at',
      ascdesc: 'DESC',
      limit: APP_VARIABLES.LIMIT,
      page: 1
    }

    it('Should be correct get ticket have all filter', async () => {
      const tickets = await ticketServices.getTickets(conditionTicket)
      expect(tickets).not.to.equal(null)
    })
  })

  describe('Create ticket', () => {
    it('Should be not null when data is correct', async () => {
      const dataTicket = {
        fullname: faker.name.findName(),
        detail: faker.lorem.text(),
        contact_information: faker.address.streetAddress(),
        status: faker.random.arrayElement(['Pending', 'Accepted', 'Resolved', 'Rejected']),
        created_at: faker.date.between('2018-01-01', '2020-12-31')
      }

      const ticket = await ticketServices.createTicket(dataTicket)

      expect(ticket).not.to.equal(null)
    })
  })

  describe('Update status ticket', () => {
    it('Should be not null when data is correct', async () => {
      const where = { status: 'Pending' }
      const getData = await TicketRepository.findTicket(where)
      const statusTicket = faker.random.arrayElement(['Pending', 'Accepted', 'Resolved', 'Rejected'])

      const ticket = await ticketServices.updateStatus(getData.id, statusTicket)
      expect(ticket).not.to.equal(null)
    })
  })
})
