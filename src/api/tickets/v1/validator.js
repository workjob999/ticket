const configVar = require('./config')

exports.requireStrWithLength = (val, length = 20) => {
  if (!val || typeof val !== 'string') {
    return false
  }

  return val.length >= 1 && val.length <= length
}

exports.isStatus = (status) => {
  if (!status || typeof status !== 'string') {
    return false
  }

  const configStatus = Object.values(configVar.STATUS)

  if (configStatus.includes(status)) {
    return true
  }

  return false
}
