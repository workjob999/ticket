const { expect } = require('chai')

const {
  requireStrWithLength,
  isStatus
} = require('./validator')

describe('[UNIT] test validator', () => {
  describe('Validator fullname', () => {
    const value = 'test'

    it('Should be equal true when correct condition', () => {
      expect(requireStrWithLength(value, 10)).to.equal(true)
    })

    it('Should be equal false when not correct condition', () => {
      expect(requireStrWithLength(value, 2)).to.equal(false)
    })
  })

  describe('Validator Status', () => {
    const listStatus = ['Pending', 'Accepted', 'Resolved', 'Rejected']

    it('Should be equal true when correct status', () => {
      listStatus.forEach((status) => {
        expect(isStatus(status)).to.equal(true)
      })
    })

    const notValidStatus = ['abc', '123', 123, null]

    it('Should be equal false when not correct status', () => {
      notValidStatus.forEach((status) => {
        expect(isStatus(status)).to.equal(false)
      })
    })
  })
})
