const HTTP_MESSAGE = require('../../../config/http-message')
const API_RESPONSE_MESSAGE = require('../../../api/response-message')
const APP_VARIABLES = require('../../../config/variables')
const TICKET_RESPONSE_MESSAGE = require('./error-message')
const ErrorHandle = require('../../../api/error')

const { requireStrWithLength, isStatus } = require('./validator')

const ErrorApiManager = require('../../error/api')
const TicketServices = require('../v1/service')

const ticketServices = new TicketServices()

exports.getTickets = async (req, res) => {
  try {
    const {
      status = null,
      startDate = null,
      endDate = null,
      limit = APP_VARIABLES.LIMIT,
      page = 1,
      order = 'created_at',
      ascdesc = 'DESC'
    } = req.query

    const conditionTicket = {
      status: status,
      startDate: startDate,
      endDate: endDate,
      order: order,
      ascdesc: ascdesc,
      limit: limit,
      page: page
    }

    const tickets = await ticketServices.getTickets(conditionTicket)

    return res.status(HTTP_MESSAGE.SUCCESS.OK.CODE).json({
      code: HTTP_MESSAGE.SUCCESS.OK.CODE,
      status: API_RESPONSE_MESSAGE.SUCCESS.MESSAGE,
      tickets
    })
  } catch (err) {
    return ErrorApiManager.handle(err, res)
  }
}

exports.createTicket = async (req, res) => {
  try {
    const {
      fullname = null,
      detail = null,
      contactInformation = null,
      status = 'Pending'
    } = req.body

    if (!requireStrWithLength(fullname, 100)) {
      throw new ErrorHandle(null, TICKET_RESPONSE_MESSAGE.INVALID_PARAMS)
    }

    if (!requireStrWithLength(detail, 255)) {
      throw new ErrorHandle(null, TICKET_RESPONSE_MESSAGE.INVALID_PARAMS)
    }

    if (!requireStrWithLength(contactInformation, 255)) {
      throw new ErrorHandle(null, TICKET_RESPONSE_MESSAGE.INVALID_PARAMS)
    }

    if (!isStatus(status)) {
      throw new ErrorHandle(null, TICKET_RESPONSE_MESSAGE.INVALID_STATUS)
    }

    const createDataTicket = {
      fullname: fullname,
      detail: detail,
      contact_information: contactInformation,
      status: status
    }

    const ticket = await ticketServices.createTicket(createDataTicket)

    return res.status(HTTP_MESSAGE.SUCCESS.OK.CODE).json({
      code: HTTP_MESSAGE.SUCCESS.OK.CODE,
      status: API_RESPONSE_MESSAGE.SUCCESS.MESSAGE,
      ticket
    })
  } catch (err) {
    return ErrorApiManager.handle(err, res)
  }
}

exports.updateStatus = async (req, res) => {
  try {
    const ticketId = req.params.ticketId
    const status = req.body.status ? req.body.status : 'Pending'

    if (!isStatus(status)) {
      throw new ErrorHandle(null, TICKET_RESPONSE_MESSAGE.INVALID_STATUS)
    }

    await ticketServices.updateStatus(ticketId, status)

    return res.status(HTTP_MESSAGE.SUCCESS.OK.CODE).json({
      code: HTTP_MESSAGE.SUCCESS.OK.CODE,
      status: API_RESPONSE_MESSAGE.SUCCESS.MESSAGE
    })
  } catch (err) {
    return ErrorApiManager.handle(err, res)
  }
}
